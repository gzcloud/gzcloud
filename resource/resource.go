package resource

import (
	gzconst "gitee.com/gzcloud/gzcloud/const"
	log2 "github.com/labstack/gommon/log"
	"gopkg.in/yaml.v2"
	"strings"

	//"github.com/jinzhu/configor"
	"io/ioutil"
	"log"
)

var Config = Resource{}

// 声明
var GzConfig *GzconfigStruct

const (
	application     = "resource/application.yml"
	applicationDev  = "resource/application-dev.yml"
	applicationProd = "resource/application-prod.yml"
	applicationTest = "resource/application-test.yml"
)

func GetConfig() *GzconfigStruct {
	// TODO 使用代码生成器，
	if GzConfig == nil {
		// 没有使用代码生成器
		readeYml(application)

		// 配置覆盖
		if strings.EqualFold(gzconst.ACTIVE_DEV, GzConfig.Active) {
			// 开发模式
			readeYml(applicationDev)
		} else if strings.EqualFold(gzconst.ACTIVE_PROD, GzConfig.Active) {
			// 线上
			readeYml(applicationProd)
		} else if strings.EqualFold(gzconst.ACTIVE_TEST, GzConfig.Active) {
			//  测试
			readeYml(applicationTest)
		}
	}
	return GzConfig
}

//*Resource
func readeYml(path string) {
	yamlFile, err := ioutil.ReadFile(path)
	if err != nil {
		log2.Error("xxxxxxxxxxx")
		log.Printf("application.Get err   #%v ", err)
	}
	err = yaml.Unmarshal(yamlFile, &Config)

	if err != nil {
		log.Printf("Unmarshal: %v", err)
	}
	GzConfig = &Config.Gz
}

type Resource struct {
	Gz GzconfigStruct
}
type GzconfigStruct struct {
	// web：通用；scf：云函数
	Mode string // "web"
	//  none（默认），dev、prod、test。分别对应 application-${Active}
	Active string
	// mode为scf时必填，作为请求路由前缀 /de
	Name string
	Log  struct {
		Level string // "error"
		Format string
		Color bool
	}
	Server struct {
		// 如 127.0.0.1
		Host string // ""
		// 如 6150
		Port string // "6150"
	}
	// 编译配置
	Build struct {
		// 如 windows
		Goos string // "windows"
		// amd64
		Goarch string // "and64"
	}
	// 中间件配置
	Middleware struct {
		Jwt struct {
			Secret  string
			Lookup  string
			Prex    string
			Ignores []string
		}
	}
	Http struct {
		Gzip bool
	}
	Cache struct {
		// all cache redis
		Type string
	}
	Datasource struct {
		Type     string
		Source   string
		MaxIdle  int16
		OpenIdle int16
	}
	Banner struct {
		Mode string // off on 隐藏、打印banner 默认on
		Text string // 自定义banner
	}
}
