module gitee.com/gzcloud/gzcloud

go 1.15

require (
	github.com/dave/jennifer v1.4.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	// github.com/labstack/echo/v4 v4.1.16
	github.com/labstack/gommon v0.3.0
	github.com/mattn/go-colorable v0.1.7 // indirect

	// 测试
	github.com/onsi/ginkgo v1.10.1
	github.com/onsi/gomega v1.7.0
	github.com/stretchr/testify v1.4.0
	github.com/tencentyun/scf-go-lib v0.0.0-20200624065115-ba679e2ec9c9
	github.com/valyala/fasttemplate v1.2.1
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
	golang.org/x/net v0.0.0-20200822124328-c89045814202
	golang.org/x/sys v0.0.0-20200826173525-f9321e4c35a6 // indirect
	golang.org/x/text v0.3.3 // indirect
	gopkg.in/yaml.v2 v2.3.0

//github.com/jinzhu/configor v1.2.0
)
