package gz

import "gitee.com/gzcloud/gzcloud/resource"

var default_config = &resource.GzconfigStruct{
	Mode:   "web", // 支持支web、scf
	Active: "dev", // prod
	// 项目名称
	Name:   "gzcloud-demo",
	Server: struct {
		Host string
		Port string
	}{Host: "127.0.0.1", Port: "8081"},
	Log: struct {
		Level  string
		Format string
		Color  bool
	}{
		Level: "debug",
		Format: `[${prefix}] - ${level} -  ${time_rfc3339_nano} - ${short_file} - ${line}`,
		Color:  true,
	},
	Banner: struct {
		Mode string // off | on 隐藏、打印banner
		Text string // 自定义banner
		//Path string
	}{
		Mode: "on", // prod时设置为 off
	},
	Build: struct {
		Goos   string
		Goarch string
	}{
		Goos:   "windows",
		Goarch: "amd64",
	},
}
