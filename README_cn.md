# gzcloud

#### 介绍
* serverless介绍 -  https://cloud.tencent.com/document/product/583



* golang的优点 - https://www.jianshu.com/p/bfe355f41f72



  > 我看中的优点：占用内存少。以SCF的HelloWorld 模板为例：nodejs：48MB、java：45-120MB、golang：10MB左右、golang（框架）：16-36MB



### 示例 

[gzcloud-cli]( https://gitee.com/gzcloud/gzcloud-cli)  正在开发



#### 软件架构

基于echo和scf-go-api-proxy源码
> echo：
>
> ## Commits on Jan 3, 2021
>
> 36f524ede8be9c0e530438a4e0c93fbc405998c9
>
> 本地更新时间：2020年8月4日18:24:20

> scf-go-api-proxy:
>
> 只更新这个目录
>
> https://github.com/linthan/scf-go-api-proxy/tree/master/core
>
> db91d6333f4d54ae5d211bf6d096abc8baf111cf
>
> 本地更新时间：2020年7月21日18:01:09

#### 使用说明

```go
// go.mod
gitee.com/gzcloud/gzcloud
```

```go
package main

import (
	gz "gitee.com/gzcloud/gzcloud"
	"gitee.com/gzcloud/gzcloud/resource"
)

func getHanlder(c gz.Context) error {
	c.JSON(200, struct {
		Name    string
		Age     int
		Student struct {
			Clazz string
			Man   string
		}
	}{Name: "小红", Age: 22, Student: struct {
		Clazz string
		Man   string
	}{Clazz: "高三", Man: "王集"}})
	return nil
}

func main() {
	app := gz.Initialize(config)
	app.GET("/", getHanlder)
    // scf: 访问 https://xxxx/demo-gz/test
	app.GET("/test", getHanlder)
	app.GET("/de", getHanlder)
	app.GET("/de/", getHanlder)

	// 判断是web还是scf
	// 如果是scf则使用适配器
    // 这个是web和scf通用，都是这一种写法，根据配置来区分。
	app.Start(":8081")
}

// 配置，重点
var config = &resource.GzconfigStruct{
	Mode:      "scf",
	Active:    "dev", // prod
	Name:      "demo-gz",// 重点，这个是scf的函数名称，如果Mode为scf，路由会把/demo-gz作为前缀
	Banner: struct {
		Mode string // off on 隐藏、打印banner
		Text string // 自定义banner
	}{
		Mode: "on", // prod时设置为 off
	},
	Build: struct {
		Goos   string
		Goarch string
	}{
		Goos:   "windows",
		Goarch: "amd64",
	},
}

```





#### 中间件 - 开发中...

* 缓存
  * https://github.com/OrlovEvgeny/go-mcache
  * https://github.com/eko/gocache
* 日志
* 数据库
* 错误处理



#### 开源协议 | License 

* echo：MIT

* scf-go-api-proxy： Apache License 2.0
* gzcloud： Apache License 2.0


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
