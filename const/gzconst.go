package gzconst

const (
	// 框架名称
	NAME = "gzcloud"
	VERSION = "0.0.1"

	ON = "on"
	OFF = "off"

	//
	ACTIVE_DEFAULT = "none"
	ACTIVE_DEV = "dev"
	ACTIVE_PROD = "prod"
	ACTIVE_TEST = "test"

	MODE_DEFAULT = "web"
	MODE_WEB = "web"
	MODE_SCF = "scf"

	EMPTY = ""

	LOG_LEVEL_INFO = "info"
	LOG_LEVEL_WARNING = "warning"
	LOG_LEVEL_DEBUG = "debug"
	LOG_LEVEL_ERROR = "error"
	LOG_LEVEL_ERR = "err"

	//BUILD_XX

)



