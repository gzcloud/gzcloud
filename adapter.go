// Packge GzLambda add Application support for the aws-severless-go-api library.
// Uses the core package behind the scenes and exposes the New method to
// get a new instance and Proxy method to send request to the gz.Application
// scf适配器 - @see 配合 /scf_proxy
package gz

import (
	stdContext "context"
	"gitee.com/gzcloud/gzcloud/scf_proxy/core"
	"github.com/tencentyun/scf-go-lib/events"
	"net/http"
)

// GzLambda makes it easy to send API Gateway proxy events to a gz.Application.
// The library transforms the proxy event into an HTTP request and then
// creates a proxy response object from the http.ResponseWriter
type GzLambda struct {
	core.RequestAccessor

	app *Application
}

// New creates a new instance of the GzLambda object.
// Receives an initialized *gz.Application object - normally created with gz.Initialize(nil).
// It returns the initialized instance of the GzLambda object.
func New(app *Application) *GzLambda {
	return &GzLambda{app: app}
}
func adapterInitialize(app *Application) *GzLambda {
	return New(app)
}

// Proxy receives an API Gateway proxy event, transforms it into an http.Request
// object, and sends it to the gz.Application for routing.
// It returns a proxy response object generated from the http.ResponseWriter.
func (e *GzLambda) Proxy(req events.APIGatewayRequest) (events.APIGatewayResponse, error) {
	echoRequest, err := e.ProxyEventToHTTPRequest(req)
	return e.proxyInternal(echoRequest, err)
}

// ProxyWithContext receives context and an API Gateway proxy event,
// transforms them into an http.Request object, and sends it to the gz.Application for routing.
// It returns a proxy response object generated from the http.ResponseWriter.
func (e *GzLambda) ProxyWithContext(ctx stdContext.Context, req events.APIGatewayRequest) (events.APIGatewayResponse, error) {
	echoRequest, err := e.EventToRequestWithContext(ctx, req)
	return e.proxyInternal(echoRequest, err)
}

func (gzLambda *GzLambda) proxyInternal(req *http.Request, err error) (events.APIGatewayResponse, error) {

	if err != nil {
		return core.GatewayTimeout(), core.NewLoggedError("Could not convert proxy event to request: %v", err)
	}

	respWriter := core.NewProxyResponseWriter()
	gzLambda.app.ServeHTTP(http.ResponseWriter(respWriter), req)

	proxyResponse, err := respWriter.GetProxyResponse()
	if err != nil {
		return core.GatewayTimeout(), core.NewLoggedError("Error while generating proxy response: %v", err)
	}

	return proxyResponse, nil
}
