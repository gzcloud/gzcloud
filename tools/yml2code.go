package tools

// 组件 https://github.com/dave/jennifer

import (
	"fmt"
	"gitee.com/gzcloud/gzcloud/resource"
	. "github.com/dave/jennifer/jen"
	"os"
)

/*
package resource

import "gitee.com/gzcloud/gzcloud/resource"

// 代码生成器生成的，指针
var GenConfig *resource.GzconfigStruct
func init()  {
	GenConfig = &resource.GzconfigStruct{}
	GenConfig.Generator = true
	GenConfig.Mode = "web"
	GenConfig.Build.Goos = "windows"
	GenConfig.Build.Goarch = "amd64"
}
*/

// yml -> code
// TODO 生成string
func yml2string() string{
	file := NewFile("resource")
	file.PackageComment("TODO 这是自动生成的文件，请不要修改.")
	// 声明参数
	file.Var().Id("GenConfig").Op("*").Qual("gitee.com/gzcloud/gzcloud/resource", "GzconfigStruct")
	fmt.Println(resource.GzConfig)
	//  声明方法
	fuc := file.Func().Id("init").Params()
	// TODO
	if false{
		fuc.Block()
	}else{
		// TODO 根据配置文件读取配置，生成文件，共5个配置
		codes := make([]Code, 5)
		// GenConfig = &resource.GzconfigStruct{}
		code1 := Id("GenConfig").Op("=").Op("&").Qual("gitee.com/gzcloud/gzcloud/resource", "GzconfigStruct").Values()
		codes[0] = code1
		// GenConfig.Generator = true
		code2 := Id("GenConfig.Generator").Op("=").True()
		codes[1] = code2
		// GenConfig.Mode = "web"
		code3 := Id("GenConfig.Mode").Op("=\"web\"")
		codes[2] = code3
		// GenConfig.Build.Goos = "windows"
		code4 := Id("GenConfig.Build.Goos").Op("=\"windows\"")
		codes[3] = code4
		// GenConfig.Build.Goarch = "amd64"
		code5 := Id("GenConfig.Build.Goarch").Op("=\"amd64\"")
		codes[4] = code5

		fuc.Block(codes...)
	}
	return file.GoString()
}

/**
根据配置文件生成
*/
func Yml2Code() error {
	str := yml2string()
	fmt.Printf(str)

	filename := "resource/GenConfig.go"
	f, err1 := os.Create(filename)
	defer f.Close()
	if err1 == nil {
		// 在后面添加
		_, err := f.Write([]byte(str))
		if err != nil {
			fmt.Printf("ioutil.WriteFile failure, err=[%v]\n", err)
		}
	} else {
		return err1
	}
	return nil
}
