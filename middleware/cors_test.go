package middleware

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"errors"
	gz "gitee.com/gzcloud/gzcloud"
	"github.com/stretchr/testify/assert"
)

func TestCORS(t *testing.T) {
	e := gz.Initialize(nil)

	// Wildcard origin
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	h := CORS()(gz.NotFoundHandler)
	h(c)
	assert.Equal(t, "*", rec.Header().Get(gz.HeaderAccessControlAllowOrigin))

	// Allow origins
	req = httptest.NewRequest(http.MethodGet, "/", nil)
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)
	h = CORSWithConfig(CORSConfig{
		AllowOrigins: []string{"localhost"},
	})(gz.NotFoundHandler)
	req.Header.Set(gz.HeaderOrigin, "localhost")
	h(c)
	assert.Equal(t, "localhost", rec.Header().Get(gz.HeaderAccessControlAllowOrigin))

	// Preflight request
	req = httptest.NewRequest(http.MethodOptions, "/", nil)
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)
	req.Header.Set(gz.HeaderOrigin, "localhost")
	req.Header.Set(gz.HeaderContentType, gz.MIMEApplicationJSON)
	cors := CORSWithConfig(CORSConfig{
		AllowOrigins:     []string{"localhost"},
		AllowCredentials: true,
		MaxAge:           3600,
	})
	h = cors(gz.NotFoundHandler)
	h(c)
	assert.Equal(t, "localhost", rec.Header().Get(gz.HeaderAccessControlAllowOrigin))
	assert.NotEmpty(t, rec.Header().Get(gz.HeaderAccessControlAllowMethods))
	assert.Equal(t, "true", rec.Header().Get(gz.HeaderAccessControlAllowCredentials))
	assert.Equal(t, "3600", rec.Header().Get(gz.HeaderAccessControlMaxAge))

	// Preflight request with `AllowOrigins` *
	req = httptest.NewRequest(http.MethodOptions, "/", nil)
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)
	req.Header.Set(gz.HeaderOrigin, "localhost")
	req.Header.Set(gz.HeaderContentType, gz.MIMEApplicationJSON)
	cors = CORSWithConfig(CORSConfig{
		AllowOrigins:     []string{"*"},
		AllowCredentials: true,
		MaxAge:           3600,
	})
	h = cors(gz.NotFoundHandler)
	h(c)
	assert.Equal(t, "localhost", rec.Header().Get(gz.HeaderAccessControlAllowOrigin))
	assert.NotEmpty(t, rec.Header().Get(gz.HeaderAccessControlAllowMethods))
	assert.Equal(t, "true", rec.Header().Get(gz.HeaderAccessControlAllowCredentials))
	assert.Equal(t, "3600", rec.Header().Get(gz.HeaderAccessControlMaxAge))

	// Preflight request with `AllowOrigins` which allow all subdomains with *
	req = httptest.NewRequest(http.MethodOptions, "/", nil)
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)
	req.Header.Set(gz.HeaderOrigin, "http://aaa.example.com")
	cors = CORSWithConfig(CORSConfig{
		AllowOrigins: []string{"http://*.example.com"},
	})
	h = cors(gz.NotFoundHandler)
	h(c)
	assert.Equal(t, "http://aaa.example.com", rec.Header().Get(gz.HeaderAccessControlAllowOrigin))

	req.Header.Set(gz.HeaderOrigin, "http://bbb.example.com")
	h(c)
	assert.Equal(t, "http://bbb.example.com", rec.Header().Get(gz.HeaderAccessControlAllowOrigin))
}

func Test_allowOriginFunc(t *testing.T) {
	returnTrue := func(origin string) (bool, error) {
		return true, nil
	}
	returnFalse := func(origin string) (bool, error) {
		return false, nil
	}
	returnError := func(origin string) (bool, error) {
		return true, errors.New("this is a test error")
	}

	allowOriginFuncs := []func(origin string) (bool, error){
		returnTrue,
		returnFalse,
		returnError,
	}

	const origin = "http://example.com"

	e := gz.New()
	for _, allowOriginFunc := range allowOriginFuncs {
		req := httptest.NewRequest(http.MethodOptions, "/", nil)
		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)
		req.Header.Set(gz.HeaderOrigin, origin)
		cors := CORSWithConfig(CORSConfig{
			AllowOriginFunc: allowOriginFunc,
		})
		h := cors(gz.NotFoundHandler)
		err := h(c)

		expected, expectedErr := allowOriginFunc(origin)
		if expectedErr != nil {
			assert.Equal(t, expectedErr, err)
			assert.Equal(t, "", rec.Header().Get(echo.HeaderAccessControlAllowOrigin))
			continue
		}

		if expected {
			assert.Equal(t, origin, rec.Header().Get(echo.HeaderAccessControlAllowOrigin))
		} else {
			assert.Equal(t, "", rec.Header().Get(echo.HeaderAccessControlAllowOrigin))
		}
	}
}
