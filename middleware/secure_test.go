package middleware

import (
	"net/http"
	"net/http/httptest"
	"testing"

	gz "gitee.com/gzcloud/gzcloud"
	"github.com/stretchr/testify/assert"
)

func TestSecure(t *testing.T) {
	e := gz.Initialize(nil)
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	h := func(c gz.Context) error {
		return c.String(http.StatusOK, "test")
	}

	// Default
	Secure()(h)(c)
	assert.Equal(t, "1; mode=block", rec.Header().Get(gz.HeaderXXSSProtection))
	assert.Equal(t, "nosniff", rec.Header().Get(gz.HeaderXContentTypeOptions))
	assert.Equal(t, "SAMEORIGIN", rec.Header().Get(gz.HeaderXFrameOptions))
	assert.Equal(t, "", rec.Header().Get(gz.HeaderStrictTransportSecurity))
	assert.Equal(t, "", rec.Header().Get(gz.HeaderContentSecurityPolicy))
	assert.Equal(t, "", rec.Header().Get(gz.HeaderReferrerPolicy))

	// Custom
	req.Header.Set(gz.HeaderXForwardedProto, "https")
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)
	SecureWithConfig(SecureConfig{
		XSSProtection:         "",
		ContentTypeNosniff:    "",
		XFrameOptions:         "",
		HSTSMaxAge:            3600,
		ContentSecurityPolicy: "default-src 'self'",
		ReferrerPolicy:        "origin",
	})(h)(c)
	assert.Equal(t, "", rec.Header().Get(gz.HeaderXXSSProtection))
	assert.Equal(t, "", rec.Header().Get(gz.HeaderXContentTypeOptions))
	assert.Equal(t, "", rec.Header().Get(gz.HeaderXFrameOptions))
	assert.Equal(t, "max-age=3600; includeSubdomains", rec.Header().Get(gz.HeaderStrictTransportSecurity))
	assert.Equal(t, "default-src 'self'", rec.Header().Get(gz.HeaderContentSecurityPolicy))
	assert.Equal(t, "", rec.Header().Get(gz.HeaderContentSecurityPolicyReportOnly))
	assert.Equal(t, "origin", rec.Header().Get(gz.HeaderReferrerPolicy))

	// Custom with CSPReportOnly flag
	req.Header.Set(gz.HeaderXForwardedProto, "https")
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)
	SecureWithConfig(SecureConfig{
		XSSProtection:         "",
		ContentTypeNosniff:    "",
		XFrameOptions:         "",
		HSTSMaxAge:            3600,
		ContentSecurityPolicy: "default-src 'self'",
		CSPReportOnly:         true,
		ReferrerPolicy:        "origin",
	})(h)(c)
	assert.Equal(t, "", rec.Header().Get(gz.HeaderXXSSProtection))
	assert.Equal(t, "", rec.Header().Get(gz.HeaderXContentTypeOptions))
	assert.Equal(t, "", rec.Header().Get(gz.HeaderXFrameOptions))
	assert.Equal(t, "max-age=3600; includeSubdomains", rec.Header().Get(gz.HeaderStrictTransportSecurity))
	assert.Equal(t, "default-src 'self'", rec.Header().Get(gz.HeaderContentSecurityPolicyReportOnly))
	assert.Equal(t, "", rec.Header().Get(gz.HeaderContentSecurityPolicy))
	assert.Equal(t, "origin", rec.Header().Get(gz.HeaderReferrerPolicy))

	// Custom, with preload option enabled
	req.Header.Set(gz.HeaderXForwardedProto, "https")
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)
	SecureWithConfig(SecureConfig{
		HSTSMaxAge:         3600,
		HSTSPreloadEnabled: true,
	})(h)(c)
	assert.Equal(t, "max-age=3600; includeSubdomains; preload", rec.Header().Get(gz.HeaderStrictTransportSecurity))

	// Custom, with preload option enabled and subdomains excluded
	req.Header.Set(gz.HeaderXForwardedProto, "https")
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)
	SecureWithConfig(SecureConfig{
		HSTSMaxAge:            3600,
		HSTSPreloadEnabled:    true,
		HSTSExcludeSubdomains: true,
	})(h)(c)
	assert.Equal(t, "max-age=3600; preload", rec.Header().Get(gz.HeaderStrictTransportSecurity))
}
