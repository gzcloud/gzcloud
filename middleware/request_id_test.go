package middleware

import (
	"net/http"
	"net/http/httptest"
	"testing"

	gz "gitee.com/gzcloud/gzcloud"
	"github.com/stretchr/testify/assert"
)

func TestRequestID(t *testing.T) {
	e := gz.Initialize(nil)
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	handler := func(c gz.Context) error {
		return c.String(http.StatusOK, "test")
	}

	rid := RequestIDWithConfig(RequestIDConfig{})
	h := rid(handler)
	h(c)
	assert.Len(t, rec.Header().Get(gz.HeaderXRequestID), 32)

	// Custom generator
	rid = RequestIDWithConfig(RequestIDConfig{
		Generator: func() string { return "customGenerator" },
	})
	h = rid(handler)
	h(c)
	assert.Equal(t, rec.Header().Get(gz.HeaderXRequestID), "customGenerator")
}
