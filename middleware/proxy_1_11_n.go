// +build !go1.11

package middleware

import (
	"net/http"
	"net/http/httputil"

	gz "gitee.com/gzcloud/gzcloud"
)

func proxyHTTP(t *ProxyTarget, c gz.Context, config ProxyConfig) http.Handler {
	return httputil.NewSingleHostReverseProxy(t.URL)
}
